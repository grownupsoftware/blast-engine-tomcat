/*
 * Grownup Software Limited.
 */
package blast.tomcat.engine;

import blast.BlastConstants;
import blast.client.WebSocketRequestDetailsImpl;
import blast.log.BlastLogger;
import java.util.List;
import java.util.Map;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * Create a WebSocketRequestDetails as part of the handshake negotiation.
 *
 * @author dhudson - Mar 31, 2017 - 10:55:19 AM
 */
public class BlastWebSocketConfigurator extends ServerEndpointConfig.Configurator {

    private static final BlastLogger logger = BlastLogger.createLogger();

    public static final String BLAST_INSTANCE_KEY = "blast.tomcat.engine";
    public static final String BLAST_REQUEST_KEY = "blast.websocket.request";

    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        WebSocketRequestDetailsImpl details = new WebSocketRequestDetailsImpl();
        details.setRequestPath(sec.getPath());
        details.setHeaders(request.getHeaders());
        details.setParams(request.getParameterMap());
        details.setRequestURI(request.getRequestURI());
        details.setQueryString(request.getQueryString());

        List<String> origins = request.getHeaders().get(BlastConstants.ORIGIN_HEADER_KEY);
        if (origins != null && !origins.isEmpty()) {
            details.setOrigin(origins.get(0));
        }

        List<String> remotes = request.getHeaders().get(BlastConstants.REMOTE_ADDRESS_KEY);
        if (remotes != null && !remotes.isEmpty()) {
            details.setRemoteAddress(remotes.get(0));
        }

        details.resolveRemoteAddress();

        // Store details on the Endpoint Config
        Map<String, Object> properties = sec.getUserProperties();
        properties.put(BLAST_REQUEST_KEY, details);
    }

}
