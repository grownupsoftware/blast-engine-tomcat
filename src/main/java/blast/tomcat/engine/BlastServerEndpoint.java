/*
 * Grownup Software Limited.
 */
package blast.tomcat.engine;

import blast.Blast;
import static blast.BlastConstants.BLAST_ROUTE_PATH;
import blast.client.ClosingReason;
import blast.client.WebSocketRequestDetails;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import blast.tomcat.client.TomcatServerClient;
import static blast.tomcat.engine.BlastWebSocketConfigurator.BLAST_INSTANCE_KEY;
import static blast.tomcat.engine.BlastWebSocketConfigurator.BLAST_REQUEST_KEY;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * Blast Server Endpoint.
 *
 * One of these is created for each client connection.
 *
 * @author dhudson - Mar 31, 2017 - 10:59:19 AM
 */
@ServerEndpoint(value = BLAST_ROUTE_PATH, configurator = BlastWebSocketConfigurator.class)
public class BlastServerEndpoint {

    private static final BlastLogger logger = BlastLogger.createLogger();

    // In a real world example this would be injected.
    private static final BlastServer blastServer = Blast.getBlastInstance(BLAST_INSTANCE_KEY);

    private TomcatServerClient client;

    public BlastServerEndpoint() {
    }

    @OnOpen
    public void onOpenCallback(Session session, EndpointConfig ec) {
        // Get the details from the end point config
        WebSocketRequestDetails details = (WebSocketRequestDetails) ec.getUserProperties().get(BLAST_REQUEST_KEY);
        client = new TomcatServerClient(session, blastServer, details);
        // Blast to manage the client
        client.postClientConnectingEvent();
    }

    @OnMessage
    public void OnMessageCallback(String messageFromClient) {
        client.queueMessage(messageFromClient.getBytes());
    }

    @OnClose
    public void onCloseCallback(Session s, CloseReason cr) {
        // TODO: Populate correctly
        client.close(ClosingReason.CLOSED_BY_SERVER);
    }

    @OnError
    public void onErrorCallback(Session s, Throwable t) {
        logger.warn("Client {} threw an Error ", s.getId(), t);
    }
}
