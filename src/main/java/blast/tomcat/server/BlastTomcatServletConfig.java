/*
 * Grownup Software Limited.
 */
package blast.tomcat.server;

import blast.tomcat.engine.BlastServerEndpoint;
import javax.servlet.ServletContextEvent;
import javax.websocket.DeploymentException;
import javax.websocket.server.ServerContainer;
import org.apache.tomcat.websocket.server.Constants;
import org.apache.tomcat.websocket.server.WsContextListener;

/**
 *
 * @author dhudson - Apr 3, 2017 - 2:34:23 PM
 */
public class BlastTomcatServletConfig extends WsContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        super.contextInitialized(sce);
        ServerContainer sc
                = (ServerContainer) sce.getServletContext().getAttribute(
                        Constants.SERVER_CONTAINER_SERVLET_CONTEXT_ATTRIBUTE);
        try {
            sc.addEndpoint(BlastServerEndpoint.class);
        } catch (DeploymentException e) {
            throw new IllegalStateException(e);
        }
    }
}
