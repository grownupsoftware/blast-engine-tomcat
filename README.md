
![Alt text](http://www.gusl.co/blast/resources/images/b-circle-trans-100.png) *on* ![Alt test](http://tomcat.apache.org/images/tomcat.png)

Built using Tomcat version 8.5.12

# Gradle
```
repositories { maven { url "http://dl.bintray.com/gusl/blast" } }

compile 'co.gusl:blast-engine-tomcat:1.0.0'
```

[ ![Download](https://api.bintray.com/packages/gusl/blast/blast-engine-tomcat/images/download.svg) ](https://bintray.com/gusl/blast/blast-engine-tomcat/_latestVersion)

# Embedded


## JSR 356

As Tomcat likes to use the JSR 356, then just include `BlastServerEndpoint` in your war.

If you require the Remote IP address of the WebSocket, then the Blast Filter will also need to be
added. See the BlastIPFilter.

# Stand alone

To run the Standalone version of blast use ..

```java
     BlastServer blast = Blast.blast(new TomcatEngine())
```
This will create a Tomcat instance and configure using the properties from Blast Properties.
As the `TomcatEngine` is a `Controllable` Blast will start and stop the engine.

## Gradle Runner
`./gradlew tomcat:tomcat-engine:run`
